package ch.stgerb.flappybird.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.fragment.app.Fragment
import ch.stgerb.flappybird.R
import ch.stgerb.flappybird.io.ScoreSafer
import kotlinx.android.synthetic.main.fragment_game_over.*
import org.json.simple.JSONArray
import org.json.simple.JSONObject
import java.io.File
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

class GameOverFragment : Fragment(), View.OnClickListener
{
    private var mScore = 0L
    private val mScoreList = ArrayList<Long>()
    private var mNewHighestScore = false

    companion object
    {
        @JvmStatic
        fun newInstance(score: Long): GameOverFragment
        {
            val fragment = GameOverFragment()
            val args = Bundle()

            args.putLong("score", score)
            fragment.arguments = args

            return fragment
        }
    }

    override fun onCreate(savedInstanceState: Bundle?)
    {
        super.onCreate(savedInstanceState)

        mScore = arguments!!.getLong("score")

        val scoreSafer = ScoreSafer()
        val file = File(activity!!.filesDir, "scores.json")
        val jsonArray: JSONArray = scoreSafer.readJSONFile(file)

        if (jsonArray.isEmpty())
        {
            if (mScore != 0L)
            {
                mNewHighestScore = true
            }

            mScoreList.add(mScore)

            writeToFile(scoreSafer, file)
        }
        else
        {
            for (value in jsonArray)
            {
                val jsonObj = value as JSONObject
                val score = jsonObj["score"] as Long

                if (!mScoreList.contains(score))
                {
                    mScoreList.add(score)
                }
            }

            Collections.sort(mScoreList, Collections.reverseOrder()) // DESC Order

            if (mScore > mScoreList[0])
            {
                mNewHighestScore = true

                mScoreList.add(0, mScore)
            }

            writeToFile(scoreSafer, file)
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View?
    {
        return inflater.inflate(R.layout.fragment_game_over, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?)
    {
        val str1 = mScore.toString()
        if (str1.length == 1)
        {
            setScore(mScore, image_score_2)
        }
        else if (str1.length == 2)
        {
            val firstChar = str1.substring(0, 1)
            setScore(firstChar.toLong(), image_score_1)

            val secondChar = str1.substring(1, 2)
            setScore(secondChar.toLong(), image_score_2)
        }

        val highestScore = mScoreList[0]
        val str2 = highestScore.toString()
        if (str2.length == 1)
        {
            setScore(highestScore, image_best_score_2)
        }
        else if (str2.length == 2)
        {
            val firstChar = str2.substring(0, 1)
            setScore(firstChar.toLong(), image_best_score_1)

            val secondChar = str2.substring(1, 2)
            setScore(secondChar.toLong(), image_best_score_2)
        }

        if (!mNewHighestScore)
        {
            image_new_pb.visibility = View.GONE

            if (mScore == 0L)
            {
                image_medal.setImageDrawable(activity!!.getDrawable(R.drawable.bronze_medal))
            }
            else
            {
                image_medal.setImageDrawable(activity!!.getDrawable(R.drawable.silver_medal))
            }
        }
        else
        {
            image_medal.setImageDrawable(activity!!.getDrawable(R.drawable.gold_medal))
        }

        button_play_again.setOnClickListener(this)
        button_high_scores.setOnClickListener(this)
    }

    private fun setScore(score: Long, imageView: ImageView)
    {
        when (score)
        {
            0L -> imageView.setImageDrawable(activity!!.getDrawable(R.drawable.num_0))
            1L -> imageView.setImageDrawable(activity!!.getDrawable(R.drawable.num_1))
            2L -> imageView.setImageDrawable(activity!!.getDrawable(R.drawable.num_2))
            3L -> imageView.setImageDrawable(activity!!.getDrawable(R.drawable.num_3))
            4L -> imageView.setImageDrawable(activity!!.getDrawable(R.drawable.num_4))
            5L -> imageView.setImageDrawable(activity!!.getDrawable(R.drawable.num_5))
            6L -> imageView.setImageDrawable(activity!!.getDrawable(R.drawable.num_6))
            7L -> imageView.setImageDrawable(activity!!.getDrawable(R.drawable.num_7))
            8L -> imageView.setImageDrawable(activity!!.getDrawable(R.drawable.num_8))
            9L -> imageView.setImageDrawable(activity!!.getDrawable(R.drawable.num_9))
        }
    }

    override fun onClick(view: View)
    {
        when (view.id)
        {
            R.id.button_play_again -> replaceFragment(GetReadyFragment())
            R.id.button_high_scores -> replaceFragment(HighScoresFragment())
        }
    }

    private fun replaceFragment(fragment: Fragment)
    {
        activity!!.supportFragmentManager.beginTransaction()
            .replace(R.id.fragment_container, fragment)
            .addToBackStack(null)
            .commit()
    }

    private fun writeToFile(scoreSafer: ScoreSafer, file: File)
    {
        val dateTime = SimpleDateFormat("yyyy-MM-dd hh:mm:ss", Locale.getDefault()).format(Date())
        val jsonObject = scoreSafer.createJSONObject(mScore, dateTime)
        scoreSafer.writeToFile(jsonObject, file)
    }
}