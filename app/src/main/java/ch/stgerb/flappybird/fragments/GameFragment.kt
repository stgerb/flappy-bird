package ch.stgerb.flappybird.fragments


import android.os.Bundle
import android.util.DisplayMetrics
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import ch.stgerb.flappybird.R
import ch.stgerb.flappybird.interfaces.CollisionListener
import ch.stgerb.flappybird.views.GameView
import kotlinx.android.synthetic.main.fragment_game.*

class GameFragment : Fragment(), CollisionListener
{
    private var mCollision = false

    override fun onCreate(savedInstanceState: Bundle?)
    {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View?
    {
        return inflater.inflate(R.layout.fragment_game, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?)
    {
        val displaySize = getDisplaySize()
        val displayWidth = displaySize["width"]
        val displayHeight = displaySize["height"]

        game_container.addView(GameView(activity!!, displayWidth!!, displayHeight!!, this))
    }

    fun getDisplaySize(): Map<String, Int>
    {
        val displayMetrics = DisplayMetrics()
        activity!!.windowManager.defaultDisplay.getMetrics(displayMetrics)
        val displaySize = HashMap<String, Int>()
        displaySize.put("width", displayMetrics.widthPixels)
        displaySize.put("height", displayMetrics.heightPixels)
        return displaySize
    }

    override fun onCollision(score: Long)
    {
        if (!mCollision)
        {
            mCollision = true

            activity!!.supportFragmentManager.beginTransaction()
                .replace(R.id.fragment_container, GameOverFragment.newInstance(score))
                .addToBackStack(null)
                .commit()
        }
    }
}
