package ch.stgerb.flappybird.views

import android.content.Context
import android.graphics.BitmapFactory
import android.graphics.Canvas
import android.graphics.Rect
import android.os.Handler
import android.view.MotionEvent
import android.view.View
import ch.stgerb.flappybird.R
import ch.stgerb.flappybird.interfaces.CollisionListener
import java.util.*

private const val UPDATE_MILLIS = 25L

class GameView(context: Context, private var mDisplayWidth: Int, private val mDisplayHeight: Int, private val mCollisionListener: CollisionListener) : View(context)
{
    private val THRESHOLD = 73 // 73 | 48 -> To customize height of bottom pipe: mDisplayHeight > canvas.height.
    private val mHandler = Handler()
    private val mRunnable = Runnable { invalidate() }
    private val mBackground = BitmapFactory.decodeResource(resources, R.drawable.background)
    private val mBase = BitmapFactory.decodeResource(resources, R.drawable.base)
    private val mPipeTop = BitmapFactory.decodeResource(resources, R.drawable.pipe_top); // TODO: Make pipes equal in Size
    private val mPipeBottom = BitmapFactory.decodeResource(resources, R.drawable.pipe_bottom); // TODO: Make pipes equal in Size
    private val mBackgroundRect = Rect(mDisplayWidth, 0, 0, mDisplayHeight)
    private val mBaseRectTop = Rect(mDisplayWidth, mBase.height, 0, 0)
    private var mBirdIndex = 0
    private val mBirdsArray = arrayOf(
        BitmapFactory.decodeResource(resources, R.drawable.bird_upflap), // TODO: Make bird bigger
        BitmapFactory.decodeResource(resources, R.drawable.bird_midflap), // TODO: Make bird bigger
        BitmapFactory.decodeResource(resources, R.drawable.bird_downflap) // TODO: Make bird bigger
    )
    private val mBirdX = mDisplayWidth / 2f - mBirdsArray[0].width / 2f
    private var mBirdY = mDisplayHeight / 2f - mBirdsArray[0].height / 2f
    private var mVelocity = 0
    private val mGravity = 3
    private val random = Random()
    private var mPipeGap = 300
    private val mMinPipeOffset = mPipeGap
    private val mMaxPipeOffset = mDisplayHeight - mMinPipeOffset - mPipeGap
    private val mNumberOfPipes = 99
    private var mWidthBetweenPipes = mDisplayWidth * 3 / 4
    private var mPipeMovementCounter = 8
    private val mPipeList = ArrayList<Map<String, Rect>>()
    private var mGameState = true
    private var mScoreCounter = 0L
    private var mScore = BitmapFactory.decodeResource(resources, R.drawable.num_0);
    private var mScoreRect1 = Rect(mBase.height, mBase.height * 2, mScore.width + mBase.height, mScore.height + (mBase.height * 2))
    private var mScoreRect2 = Rect(mScore.width + mBase.height, mBase.height * 2, (mScore.width * 2) + mBase.height, mScore.height + (mBase.height * 2))

    init
    {
        createPipes()
    }

    override fun onDraw(canvas: Canvas)
    {
        super.onDraw(canvas)

        if (mScoreCounter == 99L)
        {
            mCollisionListener.onCollision(mScoreCounter)
        }

        val baseRectBottom = Rect(width, height - mBase.height, 0, height)

        canvas.drawBitmap(mBackground, null, mBackgroundRect, null)
        canvas.drawBitmap(mBase, null, mBaseRectTop, null)
        canvas.drawBitmap(mBase, null, baseRectBottom, null)

        mBirdIndex = when (mBirdIndex)
        {
            0 -> 1
            1 -> 2
            else -> 0
        }

        if (mBirdY <= mBaseRectTop.height() + mBirdsArray[0].height) // When bird  collides with top base
        {
            mBirdY = (height.toFloat() - (height - mBaseRectTop.height())) + mBirdsArray[0].height

            mHandler.removeCallbacks(mRunnable)

            mCollisionListener.onCollision(mScoreCounter)
        }
        else if (mBirdY >= height - baseRectBottom.height() - mBirdsArray[0].height) // When bird collides with bottom base
        {
            mBirdY = height.toFloat() - baseRectBottom.height() - mBirdsArray[0].height

            mHandler.removeCallbacks(mRunnable)

            mCollisionListener.onCollision(mScoreCounter)
        }
        else // When bird does not collide
        {
            drawScore(canvas, mScoreCounter)

            drawBird(canvas)

            mVelocity += mGravity
            mBirdY += mVelocity

            if (mGameState)
            {
                for (i in 0 until mPipeList.size)
                {
                    val pipeTop = mPipeList.get(i).get("pipeTop")
                    val pipeBottom = mPipeList.get(i).get("pipeBottom")

                    pipeTop!!.left -= mPipeMovementCounter
                    pipeTop!!.right -= mPipeMovementCounter

                    pipeBottom!!.left -= mPipeMovementCounter
                    pipeBottom!!.right -= mPipeMovementCounter

                    canvas.drawBitmap(mPipeTop, null, pipeTop!!, null)
                    canvas.drawBitmap(mPipeBottom, null, pipeBottom!!, null)

                    if (mBirdY <= pipeTop.height() + mBase.height && mBirdX in pipeTop.left.toFloat()..pipeTop.right.toFloat()) // When bird collides with top pipe
                    {
                        mGameState = false
                    }
                    else if (mBirdY >= mDisplayHeight - pipeBottom.height() - mBirdsArray[0].height - mBase.height - THRESHOLD && mBirdX in pipeBottom.left.toFloat()..pipeBottom.right.toFloat()) // When bird collides with bottom pipe
                    {
                        mGameState = false

                    }
                    else if (mBirdY in pipeTop.height().toFloat() + mBase.height..mDisplayHeight - pipeBottom.height().toFloat() - mBirdsArray[0].height - mBase.height - THRESHOLD && mBirdX in pipeTop.left.toFloat()..pipeTop.right.toFloat() && mBirdX in pipeBottom.left.toFloat()..pipeBottom.right.toFloat()) // When bird flies through the pipe gap
                    {
                        if (mBirdX + 7 > pipeTop.right && mBirdX + 7 > pipeBottom.right)
                        {
                            mScoreCounter++

                            when (mScoreCounter)
                            {
                                3L ->
                                {
                                    mPipeList.clear()
                                    mPipeGap = 250
                                    mWidthBetweenPipes = mDisplayWidth * 3 / 5
                                    createPipes()
                                }
                                6L ->
                                {
                                    mPipeList.clear()
                                    mPipeGap = 200
                                    mWidthBetweenPipes = mDisplayWidth * 3 / 6
                                    createPipes()
                                }
                                9L ->
                                {
                                    mPipeList.clear()
                                    mPipeGap = 150
                                    mWidthBetweenPipes = mDisplayWidth * 3 / 7
                                    createPipes()
                                }
                            }
                        }
                    }

                }

                mHandler.postDelayed(mRunnable, UPDATE_MILLIS)
            }
            else
            {
                mHandler.removeCallbacks(mRunnable)

                mCollisionListener.onCollision(mScoreCounter)
            }
        }
    }

    override fun onTouchEvent(event: MotionEvent): Boolean
    {
        if (mGameState)
        {
            if (event.action == MotionEvent.ACTION_DOWN)
            {
                mVelocity = -30
            }
        }

        return true
    }

    fun drawBird(canvas: Canvas)
    {
        canvas.drawBitmap(
            mBirdsArray[mBirdIndex],
            mBirdX,
            mBirdY,
            null
        )
    }

    fun createPipes()
    {
        for (i in 0 until mNumberOfPipes)
        {
            val topPipeY = mMinPipeOffset + random.nextInt(mMaxPipeOffset - mMinPipeOffset + 1)

            val pipeTopRect = Rect(mDisplayWidth - mPipeTop.width + (i * mWidthBetweenPipes), mBase.height, mDisplayWidth + (i * mWidthBetweenPipes), topPipeY - (mPipeGap / 2))
            val pipeBottomRect = Rect(mDisplayWidth - mPipeBottom.width + (i * mWidthBetweenPipes), topPipeY + mPipeGap, mDisplayWidth + (i * mWidthBetweenPipes), mDisplayHeight - mBase.height - THRESHOLD)

            val map = HashMap<String, Rect>()

            map.put("pipeTop", pipeTopRect)
            map.put("pipeBottom", pipeBottomRect)

            mPipeList.add(map)
        }
    }

    fun drawScore(canvas: Canvas, score: Long)
    {
        val str = score.toString()

        if (str.length == 1)
        {
            setScore(score)
            canvas.drawBitmap(mScore, null, mScoreRect2, null)
        }
        else if (str.length == 2)
        {
            val firstChar = str.substring(0, 1)
            setScore(firstChar.toLong())
            canvas.drawBitmap(mScore, null, mScoreRect1, null)

            val secondChar = str.substring(1, 2)
            setScore(secondChar.toLong())
            canvas.drawBitmap(mScore, null, mScoreRect2, null)
        }
    }

    fun setScore(score: Long)
    {
        when (score)
        {
            0L -> mScore = BitmapFactory.decodeResource(resources, R.drawable.num_0)
            1L -> mScore = BitmapFactory.decodeResource(resources, R.drawable.num_1)
            2L -> mScore = BitmapFactory.decodeResource(resources, R.drawable.num_2)
            3L -> mScore = BitmapFactory.decodeResource(resources, R.drawable.num_3)
            4L -> mScore = BitmapFactory.decodeResource(resources, R.drawable.num_4)
            5L -> mScore = BitmapFactory.decodeResource(resources, R.drawable.num_5)
            6L -> mScore = BitmapFactory.decodeResource(resources, R.drawable.num_6)
            7L -> mScore = BitmapFactory.decodeResource(resources, R.drawable.num_7)
            8L -> mScore = BitmapFactory.decodeResource(resources, R.drawable.num_8)
            9L -> mScore = BitmapFactory.decodeResource(resources, R.drawable.num_9)
        }
    }
}