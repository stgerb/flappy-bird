package ch.stgerb.flappybird.activities

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import ch.stgerb.flappybird.R
import ch.stgerb.flappybird.fragments.*

class MainActivity : AppCompatActivity()
{
    override fun onCreate(savedInstanceState: Bundle?)
    {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_main)

        replaceFragment(MainFragment())
    }

    override fun onBackPressed()
    {
        val fragment = supportFragmentManager.findFragmentById(R.id.fragment_container)

        if (fragment is GameOverFragment)
        {
            replaceFragment(MainFragment())
        }
        else if (fragment is GetReadyFragment)
        {
            replaceFragment(MainFragment())
        }
        else if (fragment is GameFragment)
        {
            replaceFragment(MainFragment())
        }
        else if (fragment is HighScoresFragment)
        {
            replaceFragment(MainFragment())
        }
        else
        {
            finish()
        }
    }

    private fun replaceFragment(fragment: Fragment)
    {
        supportFragmentManager.beginTransaction()
            .replace(R.id.fragment_container, fragment)
            .addToBackStack(null)
            .commit()
    }
}